from datetime import date, datetime, timedelta


class Product:
	def __init__(self,name, expiryDate, postalCode, areaCode = None):
		self.name = name
		self.expiryDate = expiryDate
		self.postalCode = postalCode


	def getName(self):
		return self.name

	def getExpiryDate(self):
		return self.expiryDate

	def getPostalCode(self):
		return self.postalCode

	def __str__ (self):
		return str(self.name) +" "+str(self.expiryDate) +" "\
			+str(self.postalCode) +" "+str(self.label)



products = [
	Product("iphone", "None", "X1Y"),
	Product("orange Juice", "2018-06-20", "Y1Y"),
	Product("veggie Juice", "2019-06-20", "Z3Z"),
	Product("samsung s5", "None", "X2A"),
	Product("school bag", "None", "Y2A"),
]


class Robot:
	def __init__(self, id):
		self.id = id

class InspectionRobot(Robot):
	def __init__(self, id):
		super().__init__(self)

	def checkExpiryDate(self, product):
		expiredDate = product.getExpiryDate()
		if (expiredDate == 'None') :
			return True
		elif (datetime.strptime(expiredDate,'%Y-%m-%d') > datetime.now()) :
			return True
		else :
			return False


class DecisionRobot(Robot):
	def __init__(self, id):
		super().__init__(self)

	def getArea(self, product):
		postalCode = product.getPostalCode()
		if (postalCode == "X1Y" or postalCode == "Y1Y"):
			return 'A1'

		elif  (postalCode == "Y1X" or postalCode == "X2A"):
			return 'A2'

		elif  (postalCode == "Y2A" or postalCode == "Z3Z"):
			return 'A3'

		elif  (postalCode == "W1Y" or postalCode == "Z1W"):
			return 'A4'



class LabelingRobot(Robot):
	def __init__(self, id, areaCode):
		super().__init__(self)
		self.areaCode = areaCode

	def setLabelItem(self, product):
		product.label = self.areaCode


class Facility:
	wireHouseItems = []
	inspectionRobot = InspectionRobot("RI-01")
	decisionRobot = DecisionRobot("RD-01")
	labelingRobots = {
	'A1':LabelingRobot("RL-01", "A1"),
	'A2':LabelingRobot("RL-02", "A2"),
	'A3':LabelingRobot("RL-03", "A3"),
	'A4':LabelingRobot("RL-04", "A4")
	}
	def checkItemValidity(self):
		isValidItem = self.inspectionRobot.checkExpiryDate(product)
		return isValidItem

	def findLabeledItem(self, product):
		labeledItem = None
		if(self.checkItemValidity()):
			areaCode = self.decisionRobot.getArea(product)
			self.labelingRobots[areaCode].setLabelItem(product)
			labeledItem = product
		return labeledItem

	def addItem(self, product):
		wireHouseItemLabel = self.findLabeledItem(product)
		if wireHouseItemLabel != None:
			self.wireHouseItems.append(wireHouseItemLabel)

	def listAllItems(self):
		print ("Item, Label, Postal, Area ")
		for product in self.wireHouseItems:
			print (product)



facility = Facility()

for product in products:
	facility.findLabeledItem(product)
	facility.addItem(product)

facility.listAllItems()

